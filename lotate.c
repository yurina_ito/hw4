#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc,char *argv[]){
  char before[100];
  char after[100];
  int k,n,i,j,p;
  
  if(argc!=3){                               //引数のチェック
     perror("please input two arguments");
    exit(EXIT_FAILURE);
   }
   
   strcpy(before,argv[1]);
   printf("%s\n",before);
   
   k=atoi(argv[2]);
   n=strlen(before);
   
   if(k>=0){    //配列を右にローテンションする場合
     for(i=0;i<n;i++){
       j=(i+k)%n;
       after[j]=before[i];
     }
     printf("%s\n",after);
   }
   
   if(k<0){ //配列を左にローテンションする場合
     p=n-((-1)*k%n); //ｐだけ右にローテンションするようなpを求める

     for(i=0;i<n;i++){//配列を右にローテーション
       j=(i+p)%n;
       after[j]=before[i];
     }
     printf("%s\n",after);
   }
   
}



