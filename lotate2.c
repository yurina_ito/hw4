#include "lotate2.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void Lotate2(char *str,int x){
  char before[100];
  char after[100];
  char ret;
  int n,i,j,p;
   
   strcpy(before,str);
   printf("%s\n",before);
  
   n=strlen(before);
   
   if(x>=0){    //配列を右にローテンションする場合
     for(i=0;i<n;i++){
       j=(i+x)%n;
       after[j]=before[i];
     }
     ret=after[n];
   }
   
   if(x<0){ //配列を左にローテンションする場合
     p=n-((-1)*x%n); //ｐだけ右にローテンションするようなpを求める

     for(i=0;i<n;i++){//配列を右にローテーション
       j=(i+p)%n;
       after[j]=before[i];
     }
     ret=after[n];
   }

   return ret;
   
}
