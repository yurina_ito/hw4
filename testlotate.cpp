#include <gtest/gtest.h>

extern "C"
{
#include "lotate2.h"
}
//0のときをテスト
TEST(LotateTest,HandleZero){
  EXPECT_EQ("abcd",Lotate2("abcd",0));
}

//引数が正のときをテスト
TEST(LotateTest,Positive){
  EXPECT_EQ("defabc",Lotate2("abcdef",3));
  EXPECT_EQ("abcdef",Lotate2("abcdef",12));
  EXPECT_EQ("cdefab",Lotate2("abcdef",1000)); 
  EXPECT_EQ("cdefab",Lotate2("abcdef",10000));
}

//引数が負のときをテスト
TEST(LotateTest,Nagative){
  EXPECT_EQ("cdefab",Lotate2("abcdef",-2));
  EXPECT_EQ("abcdef",Lotate2("abcdef",-12));
  EXPECT_EQ("efabcd",Lotate2("abcdef",-1000)); 
  EXPECT_EQ("efabcd",Lotate2("abcdef",-10000));
}

//その他のテスト
TEST(LotateTest,Other){
  EXPECT_EQ("5p9wv8apdiha8",Lotate2("a85p9wv8apdih",-2));
  EXPECT_EQ("diha85p9wv8ap",Lotate2("5p9wv8apdiha8",107));
}


int main(int argc,char *argv[]){
  ::testing::InitGoogleTest(&argc,argv);
  return RUN_ALL_TESTS();
}
